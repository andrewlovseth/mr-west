<?php if(get_field('show_shop_info')): ?>
    <section class="shop-info">
        <div class="wrapper">
            
            <?php if(get_field('shop_photo')): ?>
                <div class="photo">
                    <img src="<?php $image = get_field('shop_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                </div>
            <?php endif; ?>


            <?php if(get_field('shop_copy')): ?>
                <div class="copy p2">
                    <?php the_field('shop_copy'); ?>
                </div>
            <?php endif; ?>

        </div>
    </section>
<?php endif; ?>