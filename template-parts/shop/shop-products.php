<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

    <section class="shop">
        <div class="wrapper">

            <?php the_content(); ?>
            
        </div>
    </section>

<?php endwhile; endif; ?>