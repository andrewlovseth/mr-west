<section class="main-shop">
    <div class="wrapper">
        <div class="shop-grid">

            <?php 
                $p = 1;
                $t = 1;
                if(have_rows('shop_grid')): while(have_rows('shop_grid')) : the_row(); ?>

                <?php if( get_row_layout() == 'photo' ): ?>

                    <div class="item photo photo-<?php echo $p; ?>">
                        <a href="<?php the_sub_field('link'); ?>">
                            <div class="content">
                                <img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                            </div>                        
                        </a>
                    </div>

                <?php $p++; endif; ?>

                <?php if( get_row_layout() == 'text' ): ?>

                    <div class="item text text-<?php echo $t; ?>">
                        <a href="<?php the_sub_field('link'); ?>">
                            <div class="content">
                                <div class="info">
                                    <h3><?php the_sub_field('label'); ?></h3>
                                </div>
                            </div>                        
                        </a>
                    </div>

                <?php $t++;  endif; ?>

            <?php endwhile; endif; ?>        
        
        </div>
    </div>
</section>