<nav class="shop-sub-nav">
    <div class="wrapper">
        
        <div class="links">
            <?php if(have_rows('shop_sub_nav', 'options')): while(have_rows('shop_sub_nav', 'options')): the_row(); ?>

                <?php 
                    $url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    $path = parse_url($url, PHP_URL_PATH);
                ?>
                
                <div class="link">
                    <a href="<?php the_sub_field('link'); ?>"<?php if($path == get_sub_field('link')): ?> class="active"<?php endif; ?>>
                        <?php the_sub_field('label'); ?>
                    </a>
                </div>

            <?php endwhile; endif; ?>
        </div>

    </div>
</nav>