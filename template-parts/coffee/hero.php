<?php

    $hero = get_field('hero');
    $type = $hero['type'];
    $photo = $hero['photo'];
    $video = $hero['video'];
    
?>

<section class="hero">
    <div class="wrapper">

        <?php if($type == 'photo'): ?>
            <div class="photo">
                <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
            </div>
        <?php endif; ?>

        <?php if($type == 'video'): ?>
            <div class="video">
                <?php echo $video; ?>
            </div>
        <?php endif; ?>
        
    </div>
</section>