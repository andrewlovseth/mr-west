<section class="about">
    <div class="wrapper">

        <div class="copy">
            <?php the_field('about_copy'); ?>
        </div>

        <div class="photo">
            <img src="<?php $image = get_field('about_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        </div>
    
    </div>
</section>