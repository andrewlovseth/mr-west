<section class="featured-roaster">
    <div class="wrapper">

        <div class="roaster-wrapper">

            <div class="section-header headline">
                <h2><?php the_field('section_headline'); ?></h2>
            </div>

            <div class="info">
                <div class="copy">
                    <?php the_field('roaster_copy'); ?>
                </div>
                
                <div class="cta">
                    <a href="<?php the_field('roaster_website'); ?>" class="btn" rel="external"><?php the_field('roaster_cta'); ?></a>
                </div>
            </div>

            <div class="logo">
                <a href="<?php the_field('roaster_website'); ?>" class="btn" rel="external">
                    <img src="<?php $image = get_field('roaster_logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                </a>

                <div class="timespan">
                    <h5><?php the_field('roaster_timespan'); ?></h5>
                </div>
            </div>

            <div class="gallery">
                <?php $images = get_field('roaster_gallery'); if( $images ): ?>
                    <?php foreach( $images as $image ): ?>
                        <div class="photo">
                            <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
            
        </div>

    </div>
</section>