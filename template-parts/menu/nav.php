<section class="menu-nav">
	<div class="wrapper">

		<nav class="menu-jump-nav">
			<?php if(have_rows('menus')): while(have_rows('menus')) : the_row(); ?>

				<?php if( get_row_layout() == 'menu' ): ?>

					<a href="#<?php echo sanitize_title_with_dashes(get_sub_field('name')); ?>"><?php the_sub_field('name'); ?></a>

				<?php endif; ?>

			<?php endwhile; endif; ?>
		</nav>		

	</div>
</section>

<section class="edition">
	<div class="wrapper">
		
		<div class="headline">
			<h2><?php the_field('edition_headline'); ?></h2>
		</div>	

	</div>
</section>