<?php

/*

	Template Name: Shop

*/

get_header(); ?>

	<?php get_template_part('template-parts/shop/page-header'); ?>

	<?php get_template_part('template-parts/shop/shop-nav'); ?>

	<?php get_template_part('template-parts/shop/shop-info'); ?>

	<?php get_template_part('template-parts/shop/shop-products'); ?>
	
<?php get_footer(); ?>