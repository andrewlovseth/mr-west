<?php get_header(); ?>

<section class="page-header">
	<div class="wrapper">

		<div class="headline">
			<h1>404. Page Error.</h1>
		</div>

		<div class="copy p1">
			<p>We couldn't find what you are looking for.</p>
		</div>

	</div>
</section>

	   	
<?php get_footer(); ?>