		</div> <!-- .page-wrapper -->
	</div> <!-- .page -->

	<footer class="site-footer">
		<div class="wrapper">

			<div class="site-name">
				<h1><?php bloginfo('name'); ?></h1>
			</div>

			<div class="contact-info">
				<div class="locations">
					<?php if(have_rows('locations', 'options')): while(have_rows('locations', 'options')): the_row(); ?>
					 
					    <div class="location">
					    	<a href="<?php the_sub_field('anchor'); ?>"><?php the_sub_field('name'); ?></a>
					    </div>

					<?php endwhile; endif; ?>
				</div>

				<div class="phone">
					<a href="tel:<?php the_field('phone', 'options'); ?>"><?php the_field('phone_vanity', 'options'); ?></a>
				</div>

				<div class="email">
					<a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a>
				</div>				
			</div>


			<?php if(is_page_template('covid-19.php')): ?>
				<div class="main-site-link cta">
					<a href="<?php echo site_url('/home/'); ?>" rel="external" class="btn">visit our old website</a>
				</div>

			<?php else: ?>

				<div class="gift-card">
					<div class="cta">
						<a href="<?php the_field('gift_card_link', 'options'); ?>" class="btn" rel="external">
							<?php the_field('gift_card_label', 'options'); ?>
						</a>
					</div>
				</div>

				<div class="ornament">
					<img src="<?php $image = get_field('ornament', 'options'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

			<?php endif; ?>

		</div>
	</footer>

	<?php wp_footer(); ?>

</body>
</html>