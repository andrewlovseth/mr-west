<?php

/*

	Template Name: Homepage

*/

get_header(); ?>

	<section class="hero">
		<div class="hero-logo">
			<img src="<?php $image = get_field('hero_logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>

		<div class="hero-quote">
			<div class="quote">
				<blockquote>
					<p><?php the_field('hero_quote'); ?></p>
				</blockquote>

				<cite><?php the_field('hero_quote_source'); ?></cite>				
			</div>
		</div>

		<div class="hero-image">
			<img src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />	
		</div>
	</section>

	<section class="photos">
		<?php $images = get_field('photos'); if( $images ): ?>
			<?php foreach( $images as $image ): ?>
				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php endforeach; ?>
		<?php endif; ?>
	</section>

	<section class="taglines">
		<div class="wrapper">

			<h2 class="tagline"><?php the_field('tagline_1'); ?></h2>
			<h3 class="tagline"><?php the_field('tagline_2'); ?></h3>
			
		</div>
	</section>

<?php get_footer(); ?>