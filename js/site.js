(function ($, window, document, undefined) {

	$(document).ready(function($) {


		// rel="external"
		$('a[rel="external"]').click( function() {
			window.open( $(this).attr('href') );
			return false;
		});


		// Nav Overlay Toggle
		$('.nav-trigger').click(function(){
			
			$('body').toggleClass('nav-overlay-open');

			return false;
		});


		// Smooth Scroll
		$('.menu-nav a, footer .location a').smoothScroll();


		// Override Mini Cart Link Destination
		$('.bc-mini-cart .bc-cart__continue-shopping').on('click', function(){

			console.log('CLICKED');
			window.location = "/shop/";
			return false;

		});

			
	});

})(jQuery, window, document);