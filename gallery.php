<?php

/*

	Template Name: Gallery

*/

get_header(); ?>

	<?php get_template_part('partials/content/page-header'); ?>

	<section id="gallery">
		<div class="wrapper">
			
			<div class="gallery-wrapper">
				<?php $photoIDs = ''; $galleryImages = get_field('gallery'); if( $galleryImages ): ?>
					<?php foreach( $galleryImages as $galleryImage ): ?>
						
						<?php $photoIDs .= $galleryImage['ID'] . ','; ?>

					<?php endforeach; ?>
				<?php endif; ?>

				<?php echo do_shortcode('[gallery size="thumbnail" ids="' . $photoIDs . '"]'); ?>
			</div>

			<?php if(get_field('caption')): ?>
				<div class="caption">
					<?php the_field('caption'); ?>
				</div>
			<?php endif; ?>
			
		</div>
	</section>

<?php get_footer(); ?>