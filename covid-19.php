<?php

/*

	Template Name: COVID-19

*/

get_header(); ?>

	<section class="message">
		<div class="wrapper">

			<div class="copy">
				<?php the_field('message'); ?>
			</div>

			<div class="signature">
				<?php if(get_field('cheers')): ?>
					<img src="<?php $image = get_field('cheers'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				<?php endif; ?>
				
				<span><?php the_field('cheers_names'); ?></span>
			</div>

		</div>
	</section>

	<section class="updated-hours">
		<div class="wrapper">

			<div class="illustration">
				<img src="<?php $image = get_field('updated_hours_illustration'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<div class="hours">
				<?php if(have_rows('updated_hours')): while(have_rows('updated_hours')): the_row(); ?>
 
				    <div class="entry">
				    	<div class="location">
				    		<h3><?php the_sub_field('location'); ?></h3>
				    	</div>

				    	<div class="hours">
				    		<p><?php the_sub_field('hours'); ?></p>
				    	</div>				    	
				    </div>

				<?php endwhile; endif; ?>
			</div>

		</div>
	</section>

	<section class="details">
		<div class="wrapper">
			
			<?php if(have_rows('details')): while(have_rows('details')) : the_row(); ?>
			 
			    <?php if( get_row_layout() == 'detail' ): $info = get_sub_field('info'); ?>

					<div class="detail">
						<?php if(get_sub_field('photo')): ?>
							<div class="photo">
								<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>
						<?php endif; ?>
			    		
			    		<div class="info">
			    			<?php if($info['title']): ?>
				    			<div class="header">
					    			<h3><?php echo $info['title']; ?></h3>
					    		</div>
					    	<?php endif; ?>

			    			<?php if($info['subheadline']): ?>
				    			<div class="subheadline">
					    			<h4><?php echo $info['subheadline']; ?></h4>
					    		</div>
					    	<?php endif; ?>

			    			<?php if($info['description']): ?>
				    			<div class="body">
									<?php echo $info['description']; ?>
				    			</div>
			    			<?php endif; ?>

			    			<?php $links = $info['links']; ?>

			    			<?php if($links): ?>

			    				<div class="footer cta">

				    				<?php foreach($links as $link): ?>

				    					<div class="btn-wrapper">
				    						<a class="btn" href="<?php echo $link['link']['url']; ?>" target="<?php echo $link['link']['target']; ?>"><?php echo $link['link']['title']; ?></a>
				    					</div>

									<?php endforeach; ?>

								</div>

							<?php endif; ?>

			    		</div>
					</div>
					
			    <?php endif; ?>
			 
			<?php endwhile; endif; ?>

		</div>
	</section>

	<section class="safety">
		<div class="wrapper">

			<div class="headline">
				<h3><?php the_field('safety_headline'); ?></h3>
			</div>

			<div class="copy">
				<?php the_field('safety_copy'); ?>
			</div>		

		</div>
	</section>


<?php get_footer(); ?>