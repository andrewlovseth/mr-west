<?php

/*

	Template Name: QR Menu

*/

get_header(); ?>

    <section class="qr-menu">
        <div class="wrapper">

            <?php if(have_rows('menu_links')): while(have_rows('menu_links')): the_row(); ?>

                <?php 
                    $image = get_sub_field('image');
                    $file = get_sub_field('file');
                
                ?>
    
                <div class="menu">
                    <a href="<?php echo $file['url']; ?>" rel="external">
                        <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
                    </a>
                </div>

            <?php endwhile; endif; ?>

        </div>
    </section>

<?php get_footer(); ?>