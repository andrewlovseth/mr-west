<nav class="main-mobile">
	<div class="overlay">
		<div class="overlay-wrapper">
			
			<div class="navigation">
				<?php get_template_part('partials/header/navigation'); ?>
			</div>

			<?php get_template_part('partials/header/social-links'); ?>

		</div>
	</div>
</nav>