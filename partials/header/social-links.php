<?php if(!is_page_template('covid-19.php')): ?>

	<?php if(get_field('show_cart_menu', 'options') == true): ?>
		<?php wp_nav_menu(array('menu' => 'Cart Menu')); ?>
	<?php endif; ?>

<?php endif; ?>

<div class="social-links">
	<?php if(have_rows('social_links', 'options')): while(have_rows('social_links', 'options')): the_row(); ?>

		<?php if(get_sub_field('email')): ?>
			
			<a href="mailto:<?php the_sub_field('link'); ?>">
				<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
			
		<?php else: ?>

			<a href="<?php the_sub_field('link'); ?>" rel="external">
				<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>

		<?php endif; ?>
	 
	<?php endwhile; endif; ?>
</div>