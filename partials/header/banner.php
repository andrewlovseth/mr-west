<?php if(get_field('show_banner', 'options')): ?>
	<section class="banner">
		<div class="wrapper">

			<?php if(have_rows('banners', 'options')): while(have_rows('banners', 'options')): the_row(); ?>
			 
			    <div class="banner-entry">

					<?php if(get_sub_field('link')): ?>
						<a href="<?php the_sub_field('link'); ?>">
					<?php endif; ?>

						<span class="mobile">
							<img src="<?php $image = get_sub_field('mobile'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</span>

						<span class="desktop">
							<img src="<?php $image = get_sub_field('desktop'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</span>

					<?php if(get_sub_field('link', $home)): ?>
						</a>
					<?php endif; ?>

			    </div>

			<?php endwhile; endif; ?>

		</div>
	</section>

<?php endif; ?>