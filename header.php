<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
	<div class="page">
		<div class="page-wrapper">
			
			<header class="site-header">
				<div class="wrapper">

					<?php get_template_part('partials/header/site-logo'); ?>

					<?php get_template_part('partials/header/hamburger'); ?>

					<?php get_template_part('partials/header/social-links'); ?>						

					<?php get_template_part('partials/header/order-online'); ?>

					<nav class="main-desktop">
						<?php get_template_part('partials/header/navigation'); ?>							
					</nav>

				</div>
			</header>

			<?php get_template_part('partials/header/mobile-navigation'); ?>	

			<?php get_template_part('partials/header/banner'); ?>