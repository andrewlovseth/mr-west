<?php

/*

	Template Name: Host an Event

*/

get_header(); ?>

	<?php get_template_part('partials/content/page-header'); ?>

	<section class="photos">
		<div class="wrapper">
			
			<div class="photo main">
				<img src="<?php $image = get_field('photo_main'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<div class="photo sub left">
				<img src="<?php $image = get_field('photo_sub_left'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<div class="photo sub right">
				<img src="<?php $image = get_field('photo_sub_right'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

		</div>
	</section>

	<section class="letter">
		<div class="wrapper">

			<div class="copy p3">
				<?php the_field('letter_copy'); ?>
			</div>

			<div class="signature">
				<img src="<?php $image = get_field('signature'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<div class="cta">
				<a href="<?php the_field('cta_link'); ?>" class="btn" rel="external"><?php the_field('cta_label'); ?></a>
			</div>
			
		</div>
	</section>

	<section class="experiences">
		<div class="wrapper">
			
			<div class="headline section-header">
				<h2><?php the_field('experiences_headline'); ?></h2>
			</div>

			<div class="copy p4">
				<p><?php the_field('experience_description'); ?></p>
			</div>

			<div class="options">
				<?php if(have_rows('experience_options')): while(have_rows('experience_options')): the_row(); ?>
 
				    <div class="option">
				    	<div class="photo">
				    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    	</div>

				    	<div class="info">
				    		<?php $info = get_sub_field('info'); ?>
				    		<div class="headline">
				    			<h3><?php echo $info['headline']; ?></h3>
				    		</div>

				    		<div class="copy">
				    			<?php echo $info['details']; ?>
				    		</div>
				    	</div>				        
				    </div>

				<?php endwhile; endif; ?>
			</div>

		</div>
	</section>

<?php get_footer(); ?>