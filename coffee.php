<?php

/*

	Template Name: Coffee

*/

get_header(); ?>

	<?php get_template_part('partials/content/page-header'); ?>

	<?php get_template_part('template-parts/coffee/hero'); ?>

	<?php get_template_part('template-parts/coffee/about'); ?>

	<?php get_template_part('template-parts/coffee/featured-roaster'); ?>

<?php get_footer(); ?>