<?php

/*

	Template Name: Shop Home

*/

get_header(); ?>

	<?php get_template_part('template-parts/shop/page-header'); ?>

	<?php get_template_part('template-parts/shop/shop-nav'); ?>
	
	<?php get_template_part('template-parts/shop/shop-grid'); ?>

<?php get_footer(); ?>