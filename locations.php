<?php

/*

	Template Name: Locations

*/

get_header(); ?>

	<?php get_template_part('partials/content/page-header'); ?>

	<section class="locations">
		<div class="wrapper">
			
			<?php if(have_rows('locations')): while(have_rows('locations')) : the_row(); ?>
			 
			    <?php if( get_row_layout() == 'location' ): ?>

			    	<?php $info = get_sub_field('info'); ?>

					<div class="location" id="<?php echo sanitize_title_with_dashes($info['name']); ?>">
						<div class="photo">
							<a href="<?php echo $info['google_link']; ?>" rel="external">
								<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</a>
						</div>

						<div class="info">
							<div class="headline">
								<h3><?php echo $info['name']; ?></h3>
							</div>

							<div class="directions detail">
								<h4>Get Directions</h4>
								<p><a href="<?php echo $info['google_link']; ?>" rel="external"><?php echo $info['address']; ?></a></p>
							</div>

							<div class="hours detail">
								<h4>Hours</h4>
								<p><?php echo $info['hours']; ?></p>
							</div>				
						</div>    		
					</div>
					
			    <?php endif; ?>
			 
			<?php endwhile; endif; ?>

		</div>
	</section>

	<section class="contact-info">
		<div class="wrapper">
			
			<div class="info">
				<div class="phone detail">
					<h4>Give us a ring</h4>
					<p><a href="tel:<?php the_field('phone', 'options'); ?>"><?php the_field('phone_vanity', 'options'); ?></a></p>
				</div>		

				<div class="email detail">
					<h4>General inquiries</h4>
					<p><a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a></p>
				</div>		

				<div class="events-email detail">
					<h4>To book an event</h4>
					<p><a href="mailto:<?php the_field('events_email', 'options'); ?>"><?php the_field('events_email', 'options'); ?></a></p>
				</div>		

				<div class="careers-email detail">
					<h4>Join our team!</h4>
					<p><a href="mailto:<?php the_field('careers_email', 'options'); ?>"><?php the_field('careers_email', 'options'); ?></a></p>
				</div>	
			</div>

		</div>
	</section>

<?php get_footer(); ?>